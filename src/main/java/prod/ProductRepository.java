package prod;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class ProductRepository {


    private EntityManager entityManager;

    public ProductRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void addItem(Produkt produkt) {
        entityManager.getTransaction().begin();
        entityManager.persist(produkt);
        entityManager.getTransaction().commit();
    }


    public List<Produkt> showProducts() {

        TypedQuery<Produkt> query = entityManager.createQuery("From Produkt", Produkt.class);
        return query.getResultList();
    }

    public void editProduct(Produkt produkt,int id) {
        entityManager.getTransaction().begin();
        Produkt produkt1 = entityManager.find(Produkt.class, id);
        entityManager.merge(produkt);               //persist zapisuje do bazy, hibernate aktualizuje obiekt na obiekcie. Merge odrazu działa na obiekcie i w rekordzie podmieni dane
        entityManager.getTransaction().commit();
    }

    public void deleteProduct(int id) {
        entityManager.getTransaction().begin();
        Produkt produkt = entityManager.find(Produkt.class, id);
        entityManager.remove(produkt);
        entityManager.getTransaction().commit();
    }
}

