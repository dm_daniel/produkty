package prod;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
public class Produkt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public String nazwa;
    public String kategoria;
    public BigDecimal cena;
    public Integer ocena;
    public String opis;

    @Override
    public String toString() {
        return "Produkt{" +
                "id=" + id +
                ", nazwa='" + nazwa + '\'' +
                ", kategoria='" + kategoria + '\'' +
                ", cena=" + cena +
                ", ocena=" + ocena +
                ", opis='" + opis + '\'' +
                '}';
    }
}
